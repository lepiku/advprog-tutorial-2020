package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class MysticAdventurer extends Adventurer {

        public MysticAdventurer(Guild guild) {
                this.name = "Mystic";
                this.guild = guild;
        }

        public void update() {
                switch (this.guild.getQuestType()) {
                        case "D":
                        case "E":
                                this.getQuests().add(this.guild.getQuest());
                                break;
                }
        }
}
