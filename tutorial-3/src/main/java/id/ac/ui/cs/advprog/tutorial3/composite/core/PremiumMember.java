package id.ac.ui.cs.advprog.tutorial3.composite.core;

import java.util.ArrayList;
import java.util.List;

public class PremiumMember implements Member {
    private String name;
    private String role;
    private List<Member> members;

    public PremiumMember(String name, String role) {
        this.name = name;
        this.role = role;
        this.members = new ArrayList<>();
    }

    @Override
    public void addChildMember(Member member) {
        if (this.role.equals("Master") || this.members.size() < 3) {
            this.members.add(member);
        }
    }

    @Override
    public void removeChildMember(Member member) {
        this.members.remove(member);
    }

    @Override
    public List<Member> getChildMembers() {
        return this.members;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String getRole() {
        return this.role;
    }
}
