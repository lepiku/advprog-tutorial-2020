package id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon;

public class Shield extends Weapon {
    public Shield() {
        this.weaponName = "Shield";
        this.weaponDescription = "Shield";
        this.weaponValue = 10;
    }
}
