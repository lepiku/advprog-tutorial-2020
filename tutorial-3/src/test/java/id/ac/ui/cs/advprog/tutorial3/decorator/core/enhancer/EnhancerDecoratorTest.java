package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;



public class EnhancerDecoratorTest {

    Weapon weapon1;
    Weapon weapon2;
    Weapon weapon3;
    Weapon weapon4;
    Weapon weapon5;
    @Test
    public void testAddWeaponEnhancement(){
        weapon1 = new Gun();
        weapon2 = new Longbow();
        weapon3 = new Gun();
        weapon4 = new Shield();
        weapon5 = new Sword();

        weapon1 = EnhancerDecorator.CHAOS_UPGRADE.addWeaponEnhancement(weapon1);
        weapon2 = EnhancerDecorator.MAGIC_UPGRADE.addWeaponEnhancement(weapon2);
        weapon3 = EnhancerDecorator.REGULAR_UPGRADE.addWeaponEnhancement(weapon3);
        weapon4 = EnhancerDecorator.RAW_UPGRADE.addWeaponEnhancement(weapon4);
        weapon5 = EnhancerDecorator.UNIQUE_UPGRADE.addWeaponEnhancement(weapon5);

        int value;
        value = weapon1.getWeaponValue();
        assertTrue(value >= 70 && value <= 75);
        value = weapon2.getWeaponValue();
        assertTrue(value >= 30 && value <= 35);
        value = weapon3.getWeaponValue();
        assertTrue(value >= 21 && value <= 25);
        value = weapon4.getWeaponValue();
        assertTrue(value >= 15 && value <= 20);
        value = weapon5.getWeaponValue();
        assertTrue(value >= 35 && value <= 40);
    }

}
