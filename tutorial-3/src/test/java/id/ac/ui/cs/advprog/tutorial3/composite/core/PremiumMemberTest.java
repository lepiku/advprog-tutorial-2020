package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PremiumMemberTest {
    private Member member;

    @BeforeEach
    public void setUp() {
        member = new PremiumMember("Wati", "Gold Merchant");
    }

    @Test
    public void testMethodGetName() {
        assertEquals("Wati", member.getName());
    }

    @Test
    public void testMethodGetRole() {
        assertEquals("Gold Merchant", member.getRole());
    }

    @Test
    public void testMethodAddChildMember() {
        Member testMember = new OrdinaryMember("Nini", "Merchant");
        member.addChildMember(testMember);
        assertEquals(testMember, member.getChildMembers().get(0));
    }

    @Test
    public void testMethodRemoveChildMember() {
        Member testMember = new OrdinaryMember("Nini", "Merchant");
        member.addChildMember(testMember);
        member.removeChildMember(testMember);
        assertEquals(true, member.getChildMembers().isEmpty());
    }

    @Test
    public void testNonGuildMasterCanNotAddChildMembersMoreThanThree() {
        Member master = new PremiumMember("Boss", "Master");
        master.addChildMember(new OrdinaryMember("A", "Merchant"));
        master.addChildMember(new OrdinaryMember("B", "Merchant"));
        master.addChildMember(new OrdinaryMember("C", "Merchant"));
        master.addChildMember(new OrdinaryMember("D", "Merchant"));

        assertEquals(4, master.getChildMembers().size());
    }

    @Test
    public void testGuildMasterCanAddChildMembersMoreThanThree() {
        member.addChildMember(new OrdinaryMember("A", "Merchant"));
        member.addChildMember(new OrdinaryMember("B", "Merchant"));
        member.addChildMember(new OrdinaryMember("C", "Merchant"));
        member.addChildMember(new OrdinaryMember("D", "Merchant"));

        assertEquals(3, member.getChildMembers().size());
    }
}
