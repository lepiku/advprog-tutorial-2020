package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class GuildTest {
    private Guild guild;
    private Member guildMaster;

    @BeforeEach
    public void setUp() {
        guildMaster = new PremiumMember("Eko", "Master");
        guild = new Guild(guildMaster);
    }

    @Test
    public void testMethodAddMember() {
        Member testMember = new OrdinaryMember("Nini", "Merchant");
        guild.addMember(guildMaster, testMember);

        assertEquals(testMember, guild.getMemberList().get(1));
        assertEquals(testMember, guildMaster.getChildMembers().get(0));
    }

    @Test
    public void testMethodRemoveMember() {
        Member testMember = new OrdinaryMember("Nini", "Merchant");
        guild.addMember(guildMaster, testMember);
        guild.removeMember(guildMaster, testMember);

        assertEquals(1, guild.getMemberList().size());
        assertEquals(true, guildMaster.getChildMembers().isEmpty());
    }

    @Test
    public void testMethodMemberHierarchy() {
        Member sasuke = new OrdinaryMember("Asep", "Servant");
        guild.addMember(guildMaster, sasuke);
        assertEquals(guildMaster, guild.getMemberHierarchy().get(0));
        assertEquals(sasuke, guild.getMemberHierarchy().get(0).getChildMembers().get(0));
    }

    @Test
    public void testMethodGetMember() {
        Member testMember = new OrdinaryMember("Nini", "Merchant");
        guild.addMember(guildMaster, testMember);

        assertEquals(testMember, guild.getMemberList().get(1));
    }
}
