package id.ac.ui.cs.advprog.tutorial5.service;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import id.ac.ui.cs.advprog.tutorial5.model.Soul;
import id.ac.ui.cs.advprog.tutorial5.repository.SoulRepository;
import id.ac.ui.cs.advprog.tutorial5.service.SoulServiceImpl;

@ExtendWith(MockitoExtension.class)
public class SoulServiceImplTest {

    @Mock
    private SoulRepository soulRepository;

    @InjectMocks
    private SoulServiceImpl soulService;

    @Test
    public void whenFindAllIsCalledItShouldCallSoulServiceFindAll() {
        soulService.findAll();
        verify(soulRepository, times(1)).findAll();
    }

    @Test
    public void whenFindSoulIsCalledItShouldCallSoulServiceFindById() {
        soulService.findSoul(1L);
        verify(soulRepository, times(1)).findById(1L);
    }

    @Test
    public void whenEraseIsCalledItShouldCallSoulServiceDeleteById() {
        soulService.erase(1L);
        verify(soulRepository, times(1)).deleteById(1L);
    }

    @Test
    public void whenRegisterCalledItShouldCallSoulServiceSave() {
        Soul soul = new Soul("Okto", 19, 'M', "Mahasiswa");

        soulService.register(soul);
        verify(soulRepository, times(1)).save(soul);
    }

    @Test
    public void whenRewriteCalledItShouldCallSoulServiceSave() {
        Soul soul = new Soul("Okto", 19, 'M', "Mahasiswa");

        soulService.register(soul);

        soul.setName("Kholish");
        soulService.rewrite(soul);

        verify(soulRepository, times(2)).save(soul);
    }
}
