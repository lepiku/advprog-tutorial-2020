package id.ac.ui.cs.advprog.tutorial5.model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import id.ac.ui.cs.advprog.tutorial5.model.Soul;

public class SoulTest {

    private Soul soul;

    @BeforeEach
    public void setUp() {
        soul = new Soul("Okto", 19, 'M', "Mahasiswa");
    }

    @Test
    public void testGetName() {
        assertEquals("Okto", soul.getName());
    }

    @Test
    public void testSetName() {
        soul.setName("Kholish");
        assertEquals("Kholish", soul.getName());
    }

    @Test
    public void testGetAge() {
        assertEquals(19, soul.getAge());
    }

    @Test
    public void testSetAge() {
        soul.setAge(20);
        assertEquals(20, soul.getAge());
    }

    @Test
    public void testGetGender() {
        assertEquals('M', soul.getGender());
    }

    @Test
    public void testSetGender() {
        soul.setGender('F');
        assertEquals('F', soul.getGender());
    }

    @Test
    public void testGetOccupation() {
        assertEquals("Mahasiswa", soul.getOccupation());
    }

    @Test
    public void testSetOccupation() {
        soul.setOccupation("Dosen");
        assertEquals("Dosen", soul.getOccupation());
    }
}
