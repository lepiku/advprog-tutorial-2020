package id.ac.ui.cs.advprog.tutorial5.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.bind.annotation.ResponseBody;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.handler;

import com.fasterxml.jackson.databind.ObjectMapper;

import id.ac.ui.cs.advprog.tutorial5.model.Soul;
import id.ac.ui.cs.advprog.tutorial5.service.SoulService;

@WebMvcTest(value = SoulController.class)
public class SoulControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private SoulService soulService;

    @Test
    public void findAllSouls() throws Exception {
        mockMvc.perform(get("/soul")
                .contentType("application/json"))
            .andExpect(status().isOk())
            .andExpect(handler().methodName("findAll"));
    }

    @Test
    public void createSoul() throws Exception {
        Soul soul = new Soul("Okto", 19, 'M', "Mahasiswa");

        mockMvc.perform(post("/soul")
                .contentType("application/json")
                .content(new ObjectMapper().writeValueAsString(soul)))
            .andExpect(status().isOk())
            .andExpect(handler().methodName("create"));
    }

    @Test
    public void deleteSoul() throws Exception {
        Soul soul = new Soul("Okto", 19, 'M', "Mahasiswa");

        mockMvc.perform(post("/soul")
                .contentType("application/json")
                .content(new ObjectMapper().writeValueAsString(soul)));

        mockMvc.perform(delete("/soul/" + soul.getId())
                .contentType("application/json"))
            .andExpect(status().isOk())
            .andExpect(handler().methodName("delete"));
    }
}
