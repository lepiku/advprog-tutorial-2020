package id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.Armory;

public class MetalClusterKnight extends Knight {

    public MetalClusterKnight(Armory armory) {
        this.armory = armory;
    }

    @Override
    public void prepare() {
        this.name = "Metal Cluster Knight";
        this.armor = armory.craftArmor();
        this.weapon = armory.craftWeapon();
        this.skill = armory.learnSkill();
    }
}
