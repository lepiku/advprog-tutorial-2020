package id.ac.ui.cs.advprog.tutorial4.singleton.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class HolyGrailTest {

    private HolyGrail holyGrail;

    @BeforeEach
    public void setUp() {
        holyGrail = new HolyGrail();
        holyGrail.makeAWish("I want to pass Advanced Programming.");
    }

    @Test
    public void testMakeAWish() {
        holyGrail.makeAWish("I don't want to fail Advanced Programming.");
        assertEquals("I don't want to fail Advanced Programming.", holyGrail.getHolyWish().getWish());
    }

    @Test
    public void testGetHolyWish() {
        assertEquals("I want to pass Advanced Programming.", holyGrail.getHolyWish().getWish());
    }
}
